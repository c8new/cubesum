# Aplicacion Backend Rappi - Cube summation

## Capas de la aplicacion

Esta aplicacion esta bajo el patron de diseño MVC, usando el framework YII de PHP.  

## Responsabilidades

Modelos:

*/protected/models/Matrix.php , tiene la abstraccion de las caracteristicas de la matriz descrita en el problema, asi como las funciones inherentes a ella, QUERY y UPDATE. Su constructor incluye el tamaño como parametro.

*/protected/models/CaptureForm.php , representa el formulario de captura de datos, y las funciones de validacion inherentes a este.

Controlador:

*/protected/controllers/TestController.php , crea la instancia del formulario y renderiza la vista correspondiente. Comunica a la vista con el modelo del formulario, asignando los valores del $_POST a los respectivos atributos de la instancia del modelo.  

Vista:

*/protected/views/testForm.php , incluye el HTML y la renderizacion del formulario, se encarga de la recepcion de los datos, y la visualizacion de la respuesta o error si hubiere.

## Demo

La aplicacion se puede probar en esta url:

http://www.gestiona.co/rappi/demos/backtest/

