<?php

class TestController extends CController
{

	public $defaultAction='index';

	public function actionIndex()
	{
		$formModel = new CaptureForm;

		if(isset($_POST['CaptureForm']))
		{
			$formModel->attributes = $_POST['CaptureForm'];
			if($formModel->validate())
				$formModel->printResult = true;	
		}

		$this->render('testForm', array('formModel' => $formModel));
	}


}